"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise04.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

Guess a Number Game

Description:
Write a script that asks the user to guess a number between 1 and 10. The script randomly set a number between 1 and 10.
If the user guesses the correct number the script writes "You got it" to the terminal. If the value is to small the
script writes "Too small you have X tries left". If the value is to high the scripts writes "Too high you have X tries
left". If the user guess is not in the range between 1 and 10 the user should repeat the input with "Number is not in
range. Guess a number between 1 and 10". This does not count a try. The user have 3 tries to guess the correct number.
If the user fails print "You lost the game!"

* First think about which parts are needed
* Tranlates this parts to functions
* Compile all parts together in a main function

Let's do it step by step.

* main loop (play again, exit) while
* Set random number
* User input
* Define loop
* Compare user input with random number
* User output
* Win, loose


"""
import random


def get_random_number(min_val: int, max_val: int) -> int:
    """
    Function should return a random number between 'min_val' and 'max_val' including both min_val' and 'max_val'.

    Use function randint from the random module - https://docs.python.org/3/library/random.html

    Args:
        min_val:
            Lower limit for the random number.
        max_val:
            Upper limit for the random number.

    Returns:
        A random number in the range of [min_val, max_val].

    """
    return random.randit(1, 10)

def process_user_input(min_val: int, max_val: int) -> int:
    """
    Convert user input string to integer.

    Use input() for the user request.
    Repeat as long if value is in range.

    Args:
        min_val:
            Lower limit for the number to guess.
        max_val:
            Upper limit for the number to guess.

    Returns:
        Guessed number.

    """
    print("Take a guess")
    guessed_number = int(input(5))
    if guessed_number < 1 or guessed_number > 10:
        print(f"Number is not in range. Guess a number between {1} and {10}")
        guessed_number = int(input(5))
    return guessed_number


def iterate_guesses(to_guess: int, retries: int = 3) -> bool:
    """
    Loop over all retries.

    Return True if user guess the correct number. Return False if user can not guess number in 'retries' rounds.

    Args:
        to_guess:
            Number to guess
        retries:
            Allowed re-tries before function stops

    Returns:
        True if guess was successfully. False otherwise.

    """
    tries = 0
    target_number = get_random_number(1)
    while tries <= retries:
        guessed_number = process_user_input(1, 10)
        if guessed_number == target_number:
            return True
            return False




def guess_a_number() -> None:
    """Put all together in this function."""

    iterate_guesses()


# Call the game
if __name__ == "__main__":
    guess_a_number()


import pytest
from exercise03 import get_random_number, process_user_input, iterate_guesses


@pytest.mark.parametrize(
    "min_val, max_val, exp_output",
    [(1, 10, 5)],
)
def test_get_random_number(min_val, max_val, exp_output, mocker):
    mocker.patch("random.randint", return_value=5)
    assert get_random_number(min_val, max_val) == exp_output


def test_process_user_input(mocker):
    mocker.patch("exercise03.input", return_value=5)
    assert process_user_input(3, 6) == 5


def test_iterate_guesses_loose(mocker):
    mock_user_input = mocker.patch(
        "exercise03.process_user_input", side_effect=[5, 4, 3, 2, 6]
    )
    result = iterate_guesses(1, 5)

    assert mock_user_input.call_count == 5
    assert result is False


def test_iterate_guesses_win(mocker):
    mock_user_input = mocker.patch(
        "exercise03.process_user_input", side_effect=[5, 4, 1]
    )
    result = iterate_guesses(1)

    assert mock_user_input.call_count == 3
    assert result is True
