"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise02.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

Create a Sequence Library

Create function body for following signatures:
* get_gc_content(dna_sequence: str) -> float
* get_reverse_complement(dna_sequence: str) -> str
* get_translation(rna_sequence: str) -> str
* mutate_dna(dna_sequence: str, snp_index: int, snp_nuc: str) -> str

Notes:
    All function input DNA sequences can be lower, upper or mixed case. Returned sequences are always upper case.

"""

# Codon table - 3 DNA nucleotids translate to 1 amino acid
CODON_TABLE = {
    "TTT": "F",
    "TTC": "F",
    "TTA": "L",
    "TTG": "L",
    "TCT": "S",
    "TCC": "S",
    "TCA": "S",
    "TCG": "S",
    "TAT": "Y",
    "TAC": "Y",
    "TAA": "*",
    "TAG": "*",
    "TGT": "C",
    "TGC": "C",
    "TGA": "*",
    "TGG": "W",
    "CTT": "L",
    "CTC": "L",
    "CTA": "L",
    "CTG": "L",
    "CCT": "P",
    "CCC": "P",
    "CCA": "P",
    "CCG": "P",
    "CAT": "H",
    "CAC": "H",
    "CAA": "Q",
    "CAG": "Q",
    "CGT": "R",
    "CGC": "R",
    "CGA": "R",
    "CGG": "R",
    "ATT": "I",
    "ATC": "I",
    "ATA": "I",
    "ATG": "M",
    "ACT": "T",
    "ACC": "T",
    "ACA": "T",
    "ACG": "T",
    "AAT": "N",
    "AAC": "N",
    "AAA": "K",
    "AAG": "K",
    "AGT": "S",
    "AGC": "S",
    "AGA": "R",
    "AGG": "R",
    "GTT": "V",
    "GTC": "V",
    "GTA": "V",
    "GTG": "V",
    "GCT": "A",
    "GCC": "A",
    "GCA": "A",
    "GCG": "A",
    "GAT": "D",
    "GAC": "D",
    "GAA": "E",
    "GAG": "E",
    "GGT": "G",
    "GGC": "G",
    "GGA": "G",
    "GGG": "G",
}

# Amino acids 3 letter to 1 letter lookup table
AMINO_ACIDS_3_LETTER_TO_SINGLE_LETTER = {
    "CYS": "C",
    "ASP": "D",
    "SER": "S",
    "GLN": "Q",
    "LYS": "K",
    "ILE": "I",
    "PRO": "P",
    "THR": "T",
    "PHE": "F",
    "ASN": "N",
    "GLY": "G",
    "HIS": "H",
    "LEU": "L",
    "ARG": "R",
    "TRP": "W",
    "ALA": "A",
    "VAL": "V",
    "GLU": "E",
    "TYR": "Y",
    "MET": "M",
}

AMINO_ACIDS_SINGLE_LETTER_TO_3_LETTER = dict(
    zip(
        AMINO_ACIDS_3_LETTER_TO_SINGLE_LETTER.values(),
        AMINO_ACIDS_3_LETTER_TO_SINGLE_LETTER.keys(),
    )
)


# Functions
def get_gc_content(dna_sequence: str) -> float:
    """
    Calculates the GC content in % of DNA string sequence.

    AAGcttaA -> 25.0

    :param dna_sequence:
        A DNA sequence.
    :return:
        Percentage of the letter frequency in sequence.

    """


def get_reverse_complement(dna_sequence: str) -> str:
    """
    Calculates the reverse complement sequence.

    ATGGCC -> GGCCAT

    :param dna_sequence:
        A DNA sequence.
    :return:
        Returns the reverse complement DNA sequence string

    """


def get_translation(dna_sequence: str) -> str:
    """
    Translate a DNA sequence to protein sequence.

    GAtgacGAATG -> DDE

    Notes:
        Cut sequence to last full codon triplet.

    :param dna_sequence:
        A DNA sequence.

    :return:
        A protein/peptide sequence (1-letter code).

    """


def mutate_dna(dna_sequence: str, snp_index: int, snp_nuc: str) -> str:
    """
    Mutate a single nucleotide at a specific position on the DNA sequence and return the new DNA sequence.

    ATggCCTT 4 A -> ATGGACTT

    :param dna_sequence:
        A DNA sequence.
    :param snp_index:
        Index of the SNP mutation on the 'dna_sequence'.
    :param snp_nuc:
        The nucleotide to mutate to.

    """


# Do not change anything after this line
import pytest


def test_get_gc_content():
    assert get_gc_content("aTGc") == 50
    assert get_gc_content("AAAA") == 0
    assert get_gc_content("AaaA") == 0


def test_get_gc_content_type():
    """Test if return type is from type float. """
    assert isinstance(get_gc_content("ATGC"), float)


def test_get_reverse_complement():
    assert get_reverse_complement("atgGCC") == "GGCCAT"
    assert get_reverse_complement("GCcTT") == "AAGGC"


def test_get_translation():
    assert get_translation("AUG") == "M"
    assert get_translation("AU") == ""
    assert get_translation("GAtgacGAATG") == "DDE"


def test_mutate_dna():
    assert mutate_dna("ATGccTT", 4, "a") == "ATGCATT"
    assert mutate_dna("ATGccTT", 4, "C") == "ATGCCTT"
